# autodevops on ocp

Use Runner Operator built into OCP 

or

follow this : https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster

from this :

https://gitlab.com/justinndavis/<project-name>/-/clusters/new

oc get secrets -n default

oc get secret default-token-xxxxx -o jsonpath="{['data']['ca\.crt']}" | base64 --decode

oc -n kube-system describe secret $(oc -n kube-system get secret | grep gitlab | awk '{print $1}')

to allow tiller to install managed apps :

oc new-project gitlab-managed-apps
oc adm policy add-scc-to-user anyuid -z tiller -n gitlab-managed-apps
oc adm policy add-scc-to-user privileged -z prometheus-kube-state-metrics -n gitlab-managed-apps
oc adm policy add-scc-to-user privileged -z prometheus-prometheus-server -n gitlab-managed-apps
oc adm policy add-scc-to-user privileged -z runner-gitlab-runner -n gitlab-managed-apps
oc adm policy add-scc-to-user privileged -z certmanager-cert-manager-webhook -n gitlab-managed-apps